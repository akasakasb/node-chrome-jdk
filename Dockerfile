FROM machimachi/docker-node-with-chrome

#Install Openjdk 8
RUN sh -c 'echo "deb http://ftp.debian.org/debian jessie-backports main" >> /etc/apt/sources.list.d/backports.list'
RUN apt-get update
RUN apt-get install -y --no-install-recommends -t openjdk-8-jdk-headless
